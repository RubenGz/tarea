<?php


namespace Gamma\ITP\Api;


use Gamma\ITP\Api\Data\TestCaseInterface;

interface HomeworkTesterInterface
{
    /**
     * Runs the test cases on a given homework
     * @param $homework Object A PHP class that will be evaluated
     * @param TestCaseInterface[] $testCases
     * @return TestCaseInterface[] The test cases with the populated result field
     */
    public function run($homework, array $testCases): array;

    public function displayResults(TestCaseInterface $testCase): void;
}
<?php


namespace Gamma\ITP\Homework;


class One
{
    public function isAnagram($a, $b)
    {
        $map = [];

        foreach (str_split($a) as $char) {
            $map[$char] = ($map[$char] ?? 0) + 1;
        }

        foreach (str_split($b) as $char) {
            if (!isset($map[$char]) || --$map[$char] < 0) {
                return false;
            }
        }

        return true;
    }
}
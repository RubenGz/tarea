<?php


namespace Gamma\Dogs\Models\Data;


use Gamma\Dogs\Api\Data\BreedInfoInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class BreedInfo extends AbstractSimpleObject implements BreedInfoInterface
{

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage($image): BreedInfoInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function setSubBreeds(array $subbreeds): BreedInfoInterface
    {
        return $this->setData(self::SUBBREEDS,$subbreeds);
    }

    public function getSubBreeds(): array
    {
        return $this->_get(self::SUBBREEDS);
    }

    public function setBooks(array $books): BreedInfoInterface
    {
        return $this->setData(self::BOOKS,$books);
    }

    public function getBooks(): array
    {
        return $this->_get(self::BOOKS);
    }

    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName($name): BreedInfoInterface
    {
        return $this->setData(self::NAME,$name);
    }
}
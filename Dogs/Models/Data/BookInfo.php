<?php


namespace Gamma\Dogs\Models\Data;


use Gamma\Dogs\Api\Data\BookInfoInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class BookInfo  extends AbstractSimpleObject implements BookInfoInterface
{

    public function getTitle(): string
    {
        return $this->_get(self::TITLE);
    }

    public function setTitle($title): BookInfoInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    public function getCover(): string
    {
        return $this->_get(self::COVER);
    }

    public function setCover($cover): BookInfoInterface
    {
        return $this->setData(self::COVER, $cover);
    }

    public function getIsbn(): array
    {
        return $this->_get(self::ISBN);
    }

    public function setIsbn(array $isbn): BookInfoInterface
    {
        return $this->setData(self::ISBN,$isbn);
    }

    public function getAuthorname(): array
    {
        return $this->_get(self::AUTHORNAME);
    }

    public function setAuthorname(array $authors): BookInfoInterface
    {
        return $this->setData(self::AUTHORNAME,$authors);
    }

    public function getFirstPublish(): string
    {
        return $this->_get(self::FIRSTPUBLISH);
    }

    public function setFirstPublish($firstpublish): BookInfoInterface
    {
        return $this->setData(self::FIRSTPUBLISH,$firstpublish);
    }

    public function getPublisher(): array
    {
        return $this->_get(self::PUBLISHER);
    }

    public function setPublisher(array $publisher): BookInfoInterface
    {
        return $this->setData(self::PUBLISHER,$publisher);
    }

    public function getLanguage(): array
    {
        return $this->_get(self::LANGUAGE);
    }

    public function setLanguage(array $lang): BookInfoInterface
    {
        return $this->setData(self::LANGUAGE,$lang);
    }
}
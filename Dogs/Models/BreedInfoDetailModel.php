<?php


namespace Gamma\Dogs\Models;


use Gamma\Dogs\Api\BreedInfoDetailInterface;
use Gamma\Dogs\Api\Data\BreedInfoInterface;
use Gamma\Dogs\Api\Data\BreedInfoInterfaceFactory;
use Gamma\Dogs\Api\DogsConnection;

class BreedInfoDetailModel implements BreedInfoDetailInterface
{


    protected $connection;

    protected $breedInfoFactory;

    public function __construct(
        DogsConnection $connection,
        BreedInfoInterfaceFactory $breedInfoFactory
    )
    {
        $this->connection = $connection;
        $this->breedInfoFactory = $breedInfoFactory;
    }


    public function getBreedInfo(string $breed): BreedInfoInterface
    {
        $allBreeds = $this->connection->getConnection('s/list/all');
        $breedImg = $this->connection->getConnection("/{$breed}/images/random");
        $moreInfo = $this->connection->getOpenLibrary($breed);

        $subbreeds =  array();
        foreach( $allBreeds['message'] as $key=>$value)
        {
            if( $key == $breed )
            {
                array_push($subbreeds,$value);
            }
        }

        $breedInfo = $this->breedInfoFactory->create();

        $breedInfo->setName($breed)
            ->setBooks(['holi','holi'])
            ->setImage($breedImg['message'])
            ->setSubBreeds($subbreeds)
            ->setBooks($moreInfo['docs']);
        return $breedInfo;
    }
}
<?php


namespace Gamma\Dogs\Models;


use Gamma\Dogs\Api\DogsConnection;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

//use Magento\Framework\Serialize\Serializer\Json;

class DogConnection implements DogsConnection

//list/all

//https://dog.ceo/api/breed/name/images/random

{
    protected $baseUrl;

    protected $booksUrl;

    protected $jsonSerializer;

    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://dog.ceo/api/breed',
        string $booksUrl = 'http://openlibrary.org/search.json?q='
    )
    {
        $this->baseUrl = $baseUrl;
        $this->booksUrl = $booksUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function getConnection(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrl}{$resourcePath}";
        $client = $this->httpClient->create();
        $client->get($requestPath);
        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }

    public function getGoodReeds(string $resourcePath): array
    {


    }

    public function getOpenLibrary(string $resourcePath): array
    {
        $deleteSpaces = preg_replace('/\s+/', '+', $resourcePath);
        $requestPath = "{$this->booksUrl}{$deleteSpaces}";
        $client = $this->httpClient->create();
        $client->get($requestPath);
        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }

    public function getTitleOpenLib(string $title): array
    {
        $deleteSpaces = preg_replace('/\s+/', '+', $title);
        $requestPath = "http://openlibrary.org/search.json?title={$deleteSpaces}";
        $client = $this->httpClient->create();
        $client->get($requestPath);
        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }
}
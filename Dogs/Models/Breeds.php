<?php


namespace Gamma\Dogs\Models;

use Gamma\Dogs\Api\BreedsInteface;

class Breeds implements BreedsInteface
{
    protected $connection;

    public function __construct(
        DogConnection $connection
    )
    {
        $this->connection = $connection;
    }


    public function getBreeds(): array
    {
        $data = $this->connection->getConnection('s/list/all');
        $dogBreeds = $data['message'];

        return $dogBreeds;
    }
}
<?php


namespace Gamma\Dogs\Models;


use Gamma\Dogs\Api\BookInfoDetailInterface;
use Gamma\Dogs\Api\Data\BookInfoInterface;
use Gamma\Dogs\Api\Data\BookInfoInterfaceFactory;
use Gamma\Dogs\Api\DogsConnection;

class BookInfoDetailModel implements BookInfoDetailInterface
{
    protected $connection;

    protected $bookInfoFactory;

    public function __construct(
        DogsConnection $connection,
        BookInfoInterfaceFactory $bookInfoFactory
    )
    {
        $this->connection = $connection;
        $this->bookInfoFactory = $bookInfoFactory;
    }

    public function getBookInfo(string $title): BookInfoInterface
    {
        $moreInfo = $this->connection->getTitleOpenLib($title);
        $thisBook = array();
        foreach($moreInfo['docs'] as $key => $value) {
            if ( $value['title'] == $title) {
                $thisBook = $value;
                break;
            }
        }

        $book = $this->bookInfoFactory->create();

        $book->setAuthorname($thisBook['author_name'])
            ->setCover($thisBook['cover_i'])
            ->setFirstPublish($thisBook['first_publish_year'])
            ->setIsbn($thisBook['isbn'])
            ->setLanguage($thisBook['publish_year'])
            ->setPublisher($thisBook['publisher'])
            ->setTitle($thisBook['title']);
        return $book;
    }
}
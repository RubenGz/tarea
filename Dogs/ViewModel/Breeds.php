<?php


namespace Gamma\Dogs\ViewModel;

use Gamma\Dogs\Models\BookInfoDetailModel;
use Gamma\Dogs\Models\BreedInfoDetailModel;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use \Gamma\Dogs\Models\Breeds as Bds;

class Breeds implements ArgumentInterface
{
    protected $breeds;
    protected $request;
    protected $breedsInfo;
    protected $booksInfo;


    public function __construct(
        RequestInterface $request,
        Bds $breeds,
        BreedInfoDetailModel $breedsInfo,
        BookInfoDetailModel $booksInfo
    )
    {
        $this->breeds = $breeds;
        $this->request = $request;
        $this->breedsInfo = $breedsInfo;
        $this->booksInfo = $booksInfo;
    }

    public function getBreeds(){
        $requestedParam = $this->request->getParam("breed");
        $requestedBook = $this->request->getParam('book');
        $theFullArray = array ();

       if($requestedParam){
           $theFullArray['type'] = 'breed';
           $theFullArray['info'] = $this->breedsInfo->getBreedInfo($requestedParam);
           return $theFullArray;
       }
       if($requestedBook){
           $theFullArray['type'] = 'book';
           $theFullArray['info'] = $this->booksInfo->getBookInfo($requestedBook);
           return $theFullArray;
       }


       $theFullArray['type'] = 'list';
       $theFullArray['info'] = $this->breeds->getBreeds();

        return $theFullArray;
    }

    public function woof(int $volume)
    {
        return $this->breeds->getBreeds();
    }
}
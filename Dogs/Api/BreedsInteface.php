<?php

namespace Gamma\Dogs\Api;

interface BreedsInteface{
    public function getBreeds(): array;
}
<?php

namespace Gamma\Dogs\Api\Data;

interface BreedInfoInterface{

    const IMAGE = 'image';
    const SUBBREEDS = 'subbreeds';
    const BOOKS = 'books';
    const NAME = 'name';

    public function getImage(): string;
    public function setImage($image): BreedInfoInterface;
    public function setSubBreeds(array $subbreeds): BreedInfoInterface;
    public function getSubBreeds(): array;
    public function setBooks(array $books): BreedInfoInterface;
    public function getBooks(): array;
    public function getName(): string;
    public function setName($name): BreedInfoInterface;
}
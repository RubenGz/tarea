<?php

namespace Gamma\Dogs\Api\Data;

interface BookInfoInterface{

    const TITLE = 'title';
    const COVER = 'cover';
    const ISBN = 'isbn';
    const AUTHORNAME = 'authorname';
    const FIRSTPUBLISH = 'firstpublish';
    const PUBLISHER = 'publisher';
    const LANGUAGE = 'language';


    public function getTitle(): string;
    public function setTitle($title): BookInfoInterface;
    public function getCover(): string;
    public function setCover($cover): BookInfoInterface;
    public function getIsbn():array;
    public function setIsbn(array $isbn): BookInfoInterface;
    public function getAuthorname(): array;
    public function setAuthorname(array $authors): BookInfoInterface;
    public function getFirstPublish(): string;
    public function setFirstPublish($firstpublish): BookInfoInterface;
    public function getPublisher(): array;
    public function setPublisher(array $publishPLace): BookInfoInterface;
    public function getLanguage(): array;
    public function setLanguage(array $lang): BookInfoInterface;
}
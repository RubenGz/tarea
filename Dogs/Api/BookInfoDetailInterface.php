<?php

namespace Gamma\Dogs\Api;



use Gamma\Dogs\Api\Data\BookInfoInterface;

interface BookInfoDetailInterface
{
    public function getBookInfo(string $title): BookInfoInterface;

}
<?php

namespace Gamma\Dogs\Api;


interface DogsConnection
{
    public function getConnection(string $resourcePath): array;

    public function getGoodReeds(string $resourcePath): array;

    public function getOpenLibrary(string $resourcePath): array;

    public function getTitleOpenLib(string $title): array;

}
<?php

namespace Gamma\Dogs\Api;


use Gamma\Dogs\Api\Data\BreedInfoInterface;

interface BreedInfoDetailInterface
{
    public function getBreedInfo(string $breed): BreedInfoInterface;

}
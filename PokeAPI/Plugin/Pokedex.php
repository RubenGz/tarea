<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 24/04/19
 * Time: 04:18 PM
 */

namespace Gamma\PokeAPI\Plugin;


use Gamma\PokeAPI\Api\Data\PokemonInterface;
use Gamma\PokeAPI\Api\Data\PokemonInterfaceFactory;

class Pokedex
{
    /**
     * @var PokemonInterfaceFactory
     */
    protected $pokemonInterfaceFactory;

    public function __construct(PokemonInterfaceFactory $pokemonInterfaceFactory)
    {
        $this->pokemonInterfaceFactory = $pokemonInterfaceFactory;
    }

    public function beforeGetPokemon(\Gamma\PokeAPI\Model\Pokedex $pokedex, string $name)
    {
        $lowercaseName = strtolower($name);

        return [$lowercaseName];
    }

    public function aroundGetPokemon(\Gamma\PokeAPI\Model\Pokedex $pokedex, callable $proceed, string $name) {
        if($name === 'missingno' || $name === 'missingno.') {
            $missingNo = $this->pokemonInterfaceFactory->create()
                ->setName('Missing No.')
                ->setRegion('null')
                ->setType('null')
                ->setFlavorText('How did this happen??')
                ->setImage('https://placehold.it/200/200')
                ->setMoves(['null', 'nil', 'nope.avi']);

            return $missingNo;
        }

        return $proceed($name);
    }
}